
# Outer Join

## Dependencies

```
<dependencies>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka</artifactId>
        <version>2.8.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka-streams</artifactId>
        <version>2.8.2</version>
    </dependency>
</dependencies>

```

## Topology

```
@SpringBootApplication
public class OuterJoinStreamsApplication {
    public static void main(String[] args) {
        SpringApplication.run(OuterJoinStreamsApplication.class, args);
    }

    @Bean
    public Function<KStream<String, String>, KStream<String, String>> processStreams() {
        return input -> {
            KStream<String, String> stream1 = input.filter((key, value) -> value.startsWith("Stream1:"))
                    .mapValues(value -> value.replace("Stream1:", ""));
            KStream<String, String> stream2 = input.filter((key, value) -> value.startsWith("Stream2:"))
                    .mapValues(value -> value.replace("Stream2:", ""));

            KStream<String, String> joinedStream = stream1.outerJoin(stream2,
                    (value1, value2) -> String.format("Stream1Value=%s, Stream2Value=%s", value1, value2),
                    JoinWindows.of(Duration.ofSeconds(30)),
                    Joined.with(Serdes.String(), Serdes.String(), Serdes.String())
            );

            joinedStream.foreach((key, value) -> System.out.println(String.format("Key: %s, Value: %s", key, value)));
            return joinedStream;
        };
    }
}

```

```
In this example, we define a processStreams function that takes a KStream of type String, String as input and returns a KStream of the same type. We first filter the input stream into two separate streams based on a prefix in the value (Stream1: and Stream2:). We then perform an outer join on the two streams using a 30-second window, combining the values from both streams into a single output string. Finally, we print each output value to the console and return the joined stream.

To test this application, we can publish some messages to a Kafka topic that the application is configured to consume from. For example, we can use the kafka-console-producer tool to publish messages to a topic called test:
```

## Producer 

```
$ kafka-console-producer --bootstrap-server localhost:9092 --topic test
>key1 Stream1:value1
>key2 Stream2:value2
>key3 Stream1:value3
>key4 Stream2:value4

```

## Consumer
```
$ mvn spring-boot:run
...
Key: key1, Value: Stream1Value=value1, Stream2Value=null
Key: key2, Value: Stream1Value=null, Stream2Value=value2
Key: key3, Value: Stream1Value=value3, Stream2Value=null
Key: key4, Value: Stream1Value=null, Stream2Value=value4

```