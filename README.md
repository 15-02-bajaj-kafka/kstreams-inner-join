# kstreams-inner-join



## Dependencies

```
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
        <version>2.6.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka</artifactId>
        <version>2.7.3</version>
    </dependency>
    <dependency>
        <groupId>org.apache.kafka</groupId>
        <artifactId>kafka-streams</artifactId>
        <version>3.1.1</version>
    </dependency>
</dependencies>

```

## Configuration

```
spring.kafka.consumer.bootstrap-servers=localhost:9092
spring.kafka.consumer.group-id=my-group

spring.kafka.producer.bootstrap-servers=localhost:9092

spring.kafka.streams.application-id=my-application
spring.kafka.streams.bootstrap-servers=localhost:9092
spring.kafka.streams.default-key-serde=org.apache.kafka.common.serialization.Serdes$StringSerde
spring.kafka.streams.default-value-serde=org.apache.kafka.common.serialization.Serdes$StringSerde

```

# Model class

```
public class MyModel1 {
    private String id;
    private String name;
    // getters and setters
}

public class MyModel2 {
    private String id;
    private int value;
    // getters and setters
}

public class MyModel3 {
    private String id;
    private String name;
    private int value;
    // constructor and getters
}

```

### Serdes
```
public class MyModel1Serde extends Serdes.WrapperSerde<MyModel1> {
    public MyModel1Serde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(MyModel1.class));
    }
}

public class MyModel2Serde extends Serdes.WrapperSerde<MyModel2> {
    public MyModel2Serde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(MyModel2.class));
    }
}

public class MyModel3Serde extends Serdes.WrapperSerde<MyModel3> {
    public MyModel3Serde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(MyModel3.class));
    }
}

```

### Client code

```
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafkaStreams;

@SpringBootApplication
@EnableKafkaStreams
public class KafkaStreamsJoinApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaStreamsJoinApplication.class, args);
    }

    @Bean
    public KStream<String, MyModel1> stream1(StreamsBuilder builder) {
        KStream<String, MyModel1> stream1 = builder.stream("topic1",
                Consumed.with(Serdes.String(), new MyModel1Serde()));
        return stream1;
    }

    @Bean
    public KStream<String, MyModel2> stream2(StreamsBuilder builder) {
        KStream<String, MyModel2> stream2 = builder.stream("topic2",
                Consumed.with(Serdes.String(), new MyModel2Serde()));
        return stream2;
    }

    @Bean
    public KStream<String, MyModel3> joinedStream(KStream<String, MyModel1> stream1, KStream<String, MyModel2> stream2) {
        KStream<String, MyModel3> joinedStream = stream1.join(stream2,
                (model1, model2) -> new MyModel3(model1.getId(), model1.getName(), model2.getValue()),
                JoinWindows.of(Duration.ofSeconds(10)),
                Joined.with(Serdes.String(), new MyModel1Serde(), new MyModel2Serde()));
        joinedStream.to("topic3", Produced.with(Serdes.String(), new MyModel3Serde()));
        return joinedStream;
    }
}

```

### Testing the code

```
bin/kafka-server-start.sh config/server.properties
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic topic1
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic topic2

```

- Open two terminals 

```
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic topic1 --property parse.key=true --property key.separator=,

```

```
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic topic2 --property parse.key=true --property key.separator=,

```

### inserting data
```
key1,{"id": "id1", "name": "name1"}

key2,{"id": "id2", "name": "name2"}

```

