
# Group By

## Dependencies

```
<dependencies>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka</artifactId>
        <version>2.8.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka-streams</artifactId>
        <version>2.8.2</version>
    </dependency>
</dependencies>
```

## Topology

```
@SpringBootApplication
public class KStreamGroupByExample {

    public static void main(String[] args) {
        SpringApplication.run(KStreamGroupByExample.class, args);
    }

    @Bean
    public Function<KStream<String, String>, KStream<String, Long>> processStream() {
        return input -> {
            KGroupedStream<String, String> groupedStream = input.groupBy((key, value) -> value);

            KTable<String, Long> countTable = groupedStream.count();

            KStream<String, Long> countStream = countTable.toStream();

            countStream.foreach((key, value) -> System.out.println(String.format("Key: %s, Value: %d", key, value)));

            return countStream;
        };
    }

    @Bean
    public StreamsBuilderFactoryBean streamsBuilderFactoryBean() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "kstream-groupby-example");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        StreamsConfig streamsConfig = new StreamsConfig(props);

        return new StreamsBuilderFactoryBean(streamsConfig);
    }
}

```

### Testing
```
$ kafka-console-producer --bootstrap-server localhost:9092 --topic test
>key1 value1
>key2 value2
>key3 value1
>key4 value3

```

### Response

```
$ mvn spring-boot:run
...
Key: value1, Value: 2
Key: value2, Value: 1
Key: value3, Value: 1

```