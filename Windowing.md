
# Use case

The transaction object contains txid, amount in dollars, customer name and the timestamp in datetime format. The producer is producing the transaction messages every 2 seconds. Write a complete spring boot example to demonstrate the group by and windowing functions to display the maximum transaction amoung and the corresponding customer name in a 30 minutes window 

## Dependencies

```
<dependencies>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka</artifactId>
        <version>2.8.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.kafka</groupId>
        <artifactId>spring-kafka-streams</artifactId>
        <version>2.8.2</version>
    </dependency>
</dependencies>

```

## Model class
```
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Transaction {
    private String transactionId;
    private Double amount;
    private String customerName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private LocalDateTime timestamp;

    public Transaction() {}

    public Transaction(String transactionId, Double amount, String customerName, LocalDateTime timestamp) {
        this.transactionId = transactionId;
        this.amount = amount;
        this.customerName = customerName;
        this.timestamp = timestamp;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId='" + transactionId + '\'' +
                ", amount=" + amount +
                ", customerName='" + customerName + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}

```

## Topology
```
@SpringBootApplication
public class TransactionWindowingExample {

    public static void main(String[] args) {
        SpringApplication.run(TransactionWindowingExample.class, args);
    }

    @Bean
    public Function<KStream<String, Transaction>, KStream<Windowed<String>, Transaction>> processStream() {
        return input -> {
            KStream<String, Transaction> transactions = input
                    .groupByKey()
                    .windowedBy(TimeWindows.of(Duration.ofMinutes(30)))
                    .reduce((aggValue, newValue) -> {
                        if (aggValue.getAmount() > newValue.getAmount()) {
                            return aggValue;
                        }
                        return newValue;
                    })
                    .toStream();

            transactions.foreach((key, value) -> System.out.println(String.format("Window: %s, Key: %s, Value: %s", key.window().toString(), key.key(), value)));

            return transactions;
        };
    }

    @Bean
    public StreamsBuilderFactoryBean streamsBuilderFactoryBean() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "transaction-windowing-example");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        StreamsConfig streamsConfig = new StreamsConfig(props);

        return new StreamsBuilderFactoryBean(streamsConfig);
    }
}

```
## Producer
```
$ kafka-console-producer --bootstrap-server localhost:9092 --topic transaction --property "parse.key=true" --property "key.separator=:"
>1:{"transactionId":"1", "amount":100.00, "customerName":"John Doe", "timestamp":"2022-02-16T10:00:00Z"}
>2:{"transactionId":"2", "amount":200.00, "customerName":"Jane Doe", "timestamp":"2022-02-16T10:15:00Z"}
>3:{"transactionId":"3", "amount":150.00, "customerName":"John Doe", "timestamp":"2022

```